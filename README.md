# React Redux Example

This is an example project for understanding React w/ Redux.

## Run instructions
```bash
yarn install
yarn start
```