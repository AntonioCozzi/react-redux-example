import React, { useState } from 'react';
import './App.css';
import Button from './components/Button';

function App() {
  const [title, setTitle] = useState('');

  return (
    <div className="App">
      <h1>{title}</h1>
      <Button />
      <input onChange={ev => setTitle(ev.target.value)} />
    </div>
  );
}

export default App;
