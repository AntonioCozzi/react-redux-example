import React, { Component } from 'react'
import { connect } from 'react-redux';

class SpecialText extends Component {
    render() {
        const { isOnline } = this.props;

        console.log(isOnline)

        return (
            <div>
                {isOnline.toString()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isOnline: state.onlineReducer.isOnline
    }
}

export default connect(mapStateToProps, {})(SpecialText)