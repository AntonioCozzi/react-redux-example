import React, { Component } from 'react'
import SpecialText from './SpecialText'

import { connect } from 'react-redux'
import { setIsOnline } from '../redux/actions/onlineActions'

class Button extends Component {
    render() {
        return <div style={{ border: "2px solid red" }}>
            <SpecialText />
            <button onClick={() => this.props.setIsOnline()}>Press me</button>
        </div>
    }
}

export default connect(null, { setIsOnline })(Button)