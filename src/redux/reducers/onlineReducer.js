const initialState = {
    isOnline: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_IS_ONLINE":
            return { ...state, isOnline: !state.isOnline }
    
        default:
            return state
    }
}